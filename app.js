$(document).ready(function () {

  app.render({
    source: $('#sidebar-tpl'),
    destination: $('.sidebar'),
    data: {menu: app.menu},
  })

  $('.nav-group-item').click(function () {
    $('.nav-group-item').removeClass('active')
    $(this).addClass('active')
  })

  $('.nav-group-item').eq(0).click()

})