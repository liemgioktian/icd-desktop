var app     = {}
app.menu    = []
app.name    = 'icd'
app.version = '1.0'
app.db      = new Dexie(app.name.split(' ').join('-') + 'v' + app.version)

app.render = function (opt) {
  var template = Handlebars.compile(opt.source.html())
  var result   = template(opt.data)
  if (opt.append) opt.destination.append(result)
  else if (opt.prepend) opt.destination.prepend(result)
  else opt.destination.html(result)
}

app.action = function (buttons) {
  app.render({
    source: $('#toolbar-button'),
    destination: $('.toolbar-header .toolbar-actions'),
    data: {button: buttons}
  })
}

app.model = function (table, fields) {
  this.table = table
  this.fields= fields

  this.list = function () {
    app.action([{class: 'btn-primary btn-add', icon: 'plus', label: 'Input Data', onclick: 'app[\''+table+'\'].add()'}])
    var records = []
    app.db[table].each(function (record) {
      record.action = '\
      <button class="btn btn-primary" onclick="app[\''+table+'\'].edit('+record.id+')">\
        <span class="icon icon-pencil"></span>\
      </button>\
      <button class="btn btn-negative" onclick="app[\''+table+'\'].remove('+record.id+')">\
        <span class="icon icon-trash"></span>\
      </button>'
      records.push(record)
    })
    .then(function () {
      var thead = []
      for (var r in records[0]) thead.push({sTitle: r, mData: r})
      $('.padded-more').html('<table class="'+table+'"></table>')
      $('table.' + table).DataTable({
        'aaData': records,
        'aoColumns': thead
      })
    })
  }

  this.add = function () {
    app.render({
      source: $('#form-tpl'),
      destination: $('.padded-more'),
      data: {
        fields: fields
      }
    })
    app.action([
      {class: 'btn-warning btn-cancel', label: 'Batal', icon: 'cancel', onclick: 'app[\''+table+'\'].list()'},
      {class: 'btn-primary btn-save', label: 'Simpan', icon:'floppy', onclick: 'app[\''+table+'\'].create()'},
    ])
  }

  this.create = function () {
    var obj = {}
    $('form').find('input,select,:radio').each(function () {
      obj[$(this).attr('name')] = $(this).val()
    })
    app.db[table].add(obj).then(function () {
      app[table].list()
    })
  }

  this.edit = function (id) {
    app.db[table].get(id, function (record) {
      for (var f in fields) fields[f].value = record[fields[f].field]
      app.render({
        source: $('#form-tpl'),
        destination: $('.padded-more'),
        data: {fields: fields}
      })
      app.action([
        {class: 'btn-warning btn-cancel', label: 'Batal', icon: 'cancel', onclick: 'app[\''+table+'\'].list()'},
        {class: 'btn-primary btn-save', label: 'Simpan', icon:'floppy', onclick: 'app[\''+table+'\'].update(\''+id+'\')'},
      ])
    })
  }

  this.update = function (id) {
    var obj = {id: parseInt(id)}
    $('form').find('input,select,:radio').each(function () {
      obj[$(this).attr('name')] = $(this).val()
    })
    console.log(obj)
    app.db[table].put(obj).then(function () {
      app[table].list()
    })
  }

  this.remove = function (id) {
    app.action([
      {class: 'btn-warning btn-cancel', label: 'Batal', icon: 'cancel', onclick: 'app[\''+table+'\'].list()'},
      {class: 'btn-negative btn-remove', label: 'Yakin Hapus Item', icon: 'trash', onclick: 'app[\''+table+'\'].remove_sure('+id+')'},
    ])
  }

  this.remove_sure = function (id) {
    app.db[table].where('id').equals(id).delete().then(app[table].list)
  }
}