app.menu.push({icon: 'users', label: 'Pasien', active: '', name: 'pasien', onclick: 'app.pasien.list()'})
app.db.version(app.version).stores({pasien: '++id,nip,name,ttl,gender,gol,pangkat,unitkerja,hub'})
var fields = [
  {field: 'name', label: 'Nama', value: ''},
  {field: 'gender', label: 'Jenis Kelamin', value: ''},
  {field: 'ttl', label: 'Tanggal Lahir', value: ''},
  {field: 'hub', label: 'Hubungan dg Pegawai', value: ''},
  {field: 'nip', label: 'Nomor Pegawai', value: ''},
  {field: 'gol', label: 'Golongan', value: ''},
  {field: 'pangkat', label: 'Pangkat', value: ''},
  {field: 'unitkerja', label: 'Unit Kerja', value: ''},
]
app.pasien = new app.model('pasien', fields)

app.pasien.list = function () {
  app.action([{class: 'btn-primary btn-add', icon: 'plus', label: 'Input Data', onclick: 'app.pasien.add()'}])
  var records = []
  var no = 1
  app.db.pasien.each(function (record) {
    var row = {
      'No': no,
      'Nama Lengkap': record.name,
      'Tanggal Lahir': record.ttl,
      'Status': 'Pegawai',
      'Golongan': record.gol,
      'Pangkat': record.pangkat,
      'Unit Kerja': record.unitkerja,
      '': '<button class="btn btn-primary" onclick="app.pasien.edit('+record.id+')">\
            <span class="icon icon-pencil"></span>\
          </button>\
          <button class="btn btn-negative" onclick="app.pasien.remove('+record.id+')">\
            <span class="icon icon-trash"></span>\
          </button>'
    }
    records.push(row)
    no++
  })
  .then(function () {
    var thead = []
    for (var r in records[0]) thead.push(r)
    app.render({
      source: $('#table-tpl'),
      destination: $('.padded-more'),
      data: {
        thead: thead,
        tbody: records
      }
    })
    $('table').DataTable()
  })
}

app.pasien.add = function () {
  app.render({
    source: $('#form-tpl'),
    destination: $('.padded-more'),
    data: {fields: app.pasien.fields}
  })
  app.action([
    {class: 'btn-warning btn-cancel', label: 'Batal', icon: 'cancel', onclick: 'app.pasien.list()'},
    {class: 'btn-primary btn-save', label: 'Simpan', icon:'floppy', onclick: 'app.pasien.create()'},
  ])
  $('[name="ttl"]').datepicker({changeYear: true, changeMonth: true, yearRange: '1940:2020', dateFormat: 'dd-mm-yy'})
}