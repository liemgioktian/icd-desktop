app.menu.push({icon: 'docs', label: 'Diagnosa', active: '', name: 'diagnosa', onclick: 'app.diagnosa.list()'})
app.db.version(app.version).stores({diagnosa: '++id,code,spec,type'})
var fields = [
  {field: 'code', label: 'Code', value: ''},
  {field: 'spec', label: 'Specification', value: ''},
  {field: 'type', label: 'Type', value: ''},
]
app.diagnosa = new app.model('diagnosa', fields)